# Vagrant Tutorial
Exploring the various uses of vagrant for provisioning VMs


Contents
- [Basic usage](#basic-usage)
- [Advanced Usages](#advanced-usages)

---
---

# Basic Usage

This is a walkthrough of getting running with Vagrant (http://www.vagrantup.com/)

Vagrant, by default, uses VirtualBox VMs (https://www.virtualbox.org)

## Installation
Install Virtual Box, then install vagrant. 

Test Vagrant with

	$ vagrant

## Project Setup
Vagrant requires 2 main components to run. First is the Vagrantfile config. Second is the machine image you're intending to run.

For this example we'll use Ubuntu 12.04 LTS (Precise Pangolin). Vagrant already has this box (VM) created. 

### 1) Get the box - vagrant add
First we need to get a copy of the box/VM image and associate it to an alias for later use

	$ vagrant box add precise32 http://files.vagrantup.com/precise32.box
	
Once that completes you will have a local instance of the box/VM and an alias called precise32

You can view the available local boxes with 

	$ vagrant box list

### 2) Create a project directory
One of the great things about vagrant is that it's based on a single configuration file that you can check in to source control. This allows everyone else to grab the correct VM image along with code or project they might be working on. Ultimatly they can simply checkout one project run ***vagrant up*** and have an entire multi machine infrastructure running locally. 

You can clone this repo and use the resulting directory, or start from scratch.

Lets create a project to hold our work for vagrant.

	$ mkdir vagrant-tutorial
	$ cd vagrant-tutorial
	
Here you may want to **$ git init** to version your files for later. From here on out we'll be workign within this directory 



### 3) Configure Vagrant
There are two methods to create a vagrant project. the first is to tell vagrant to initialize a project for you. The second is to create the config yourself and run it. 

#### Method 1 - vagrant init
First we'll have Vagrant initialize things for us. Here we'll use the _init_ command with the alias of the image we want to use (precise32)

	$ vagrant init precise32
	
This will create a ***Vagrantfile*** in your current directory. Take a look at the file

	$ vim Vagrantfile
	
You'll notice a variety of configuration examples provided. Feel free to modify these as needed

#### Method 2 - Hand code
The init method simply provides a base configuration for us. We can create one by hand if we want to be cleaner and more precise.

First delete the Vagrantfile create in Mehtod 1 **$ rm Vagrantfile**

Lets add some hand coded configs

	$ vim Vagrantfile
	

And add the most basic config possible

	Vagrant::Config.run do |config|
  		config.vm.box = "precise32"
  	end
  	
Thats it now lets try it out

### 4) Running the instances
Great now we're all ready to start up our machine instance. Simply call
	
	$ vagrant up
	
and your instance will begin starting up. Once it's started you can use vagrant's ssh utility for simplified access

	$ vagrant ssh
	
Great now you're working in your Ubuntu 12.04 LTS instance!

One important note is that vagrant mounts the project directory of the host machine, within the new machine instance itself. You can red and write to this directory from the machine instance or host machine and all updates will be seen by both. Take a look

	vagrant@precise32:~$ cd /vagrant
	vagrant@precise32:/vagrant$ ls
		README.md  Vagrantfile
	vagrant@precise32:/vagrant$    
	
Now lets shut it all down. 

	vagrant@precise32:/vagrant$ exit
	logout
	Connection to 127.0.0.1 closed.
	
	$ vagrant destroy
	Are you sure you want to destroy the 'default' VM? [Y/N] y
	[default] Forcing shutdown of VM...
	[default] Destroying VM and associated drives...
	$
	
Alternativly you can call this with the -f option and force the destroy

	$ vagrant destroy -f
	
Thats it. Want to use the machine again, simply call **vagrant up**

# Advanced Usages

## Box URL
config.vm.box_url - The URL that the configured box can be found at. If the box is not installed on the system, it will be retrieved from this URL when vagrant up is run.

	Vagrant::Config.run do |config|
  		config.vm.box = "precise32"
  		config.vm.box_url = "http://files.vagrantup.com/precise32.box"
  	end	

## Multiple Machines
Rather than creating separate projects for each type of machine in your stack, you can easily define multiple machine types within one Vagrantfile. Here is a simplified config starting two instances

	Vagrant::Config.run do |config|
    
    	config.vm.define :web do |config|
        	config.vm.box = "precise32"
    	end

    	config.vm.define :db do |config|
        	config.vm.box = "precise32"
    	end

	end 

Here we define two instances, one named web and one named db. With this config, running $ vagrant up will initialize and start both machines. 

Now that we have both machines running though, vagrant ssh requires an additional param indicating which machine to log into.

	$ vagrant ssh web
	
## Networking
To enable networking between the VMs and host you should define specific networking parameters. 

### Host only
In this model a private network is create allowing communicaitons between the VMs and the host, but not other machines on your network. this can be accomplished by specifying the web_config.vm.network param 

	Vagrant::Config.run do |config|
  		config.vm.define :web do |web_config|
    		# ...
    		web_config.vm.network :hostonly, "192.168.1.10"
  		end
  	
  		config.vm.define :db do |db_config|
    		# ...
    		db_config.vm.network :hostonly, "192.168.1.11"
  		end
	end



### Bridge Network
In this model the VMs request an IP from the DHCP server. With bridged networking the VMs will appear on your network like any other device. The parameters for the Vagrentfile for the bridged networking are

	Vagrant::Config.run do |config|
		config.vm.network :bridged
	end
	



	

